import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Subscription} from 'rxjs';
import {GameObject} from '../../model/GameObject';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(public httpClient: HttpClient) {
  }

  public pingServer() {
    return this.httpClient.get(environment.baseApi + 'pings/?k=' + environment.initKey + '&t0=' + Date.now());
  }


  public clearGameQueue() {
    return this.httpClient.delete(environment.baseApi + 'players/' + environment.gameObjKey + '?k=' + environment.gameObjKey);
  }

  public loadGameInfo() {
    return this.httpClient.get(environment.baseApi + 'players/' + environment.gameObjKey + '?k=' + environment.gameObjKey);
  }

  public getFirstMessageGameQueue() {
    return this.httpClient.get(environment.baseApi + 'msgs?k=' + environment.gameObjKey + '&timeout=5');
  }

  public uploadGame(game: GameObject) {
    return this.httpClient.post(
      environment.baseApi + 'msgs?k=' + environment.gameObjKey + '&to=' + environment.gameObjKey + '&data=' + JSON.stringify(game),
      null
    );
  }

 /* public pushPosition(otherPlayerKey, positions) {
    return this.httpClient.post(
      environment.baseApi + 'msgs?k=' + environment.initKey + '&to=' + otherPlayerKey + '&data=' + JSON.stringify(positions),
      null
    );
  }*/
  public pushPosition(otherPlayerKey, grid) {
    return this.httpClient.post(
      environment.baseApi + 'msgs?k=' + environment.initKey + '&to=' + otherPlayerKey + '&data=' + grid,
      null
    );
  }

  public loadGameIdInfo() {
    return this.httpClient.get(environment.baseApi + 'players/' + environment.gameIdKey + '?k=' + environment.gameIdKey);
  }

  public getGameId() {
    return this.httpClient.get(environment.baseApi + 'msgs?k=' + environment.gameIdKey + '&timeout=5');
  }

  public setGameId(id: number) {
    return this.httpClient.post(
      environment.baseApi + 'msgs?k=' + environment.gameIdKey + '&to=' + environment.gameIdKey + '&data=' + id,
      null
    );
  }

  public sendMessage(playerKey, message) {
    return this.httpClient.post(
      environment.baseApi + 'msgs?k=' + playerKey + '&to=' + playerKey + '&data=' + message,
      null
    );
  }

  public loadPlayerQueueInfo(playerkey) {
    return this.httpClient.get(environment.baseApi + 'players/' + playerkey + '?k=' + playerkey);
  }

  public getPlayerFirstQueueMessage(playerKey) {
    return this.httpClient.get(environment.baseApi + 'msgs?k=' + playerKey + '&timeout=5');
  }

  public clearPlayerQueue(playerKey) {
    return this.httpClient.delete(environment.baseApi + 'players/' + playerKey + '?k=' + playerKey);
  }

  public sendEmptyMessage(playerKey) {
    return this.httpClient.post(
      environment.baseApi + 'msgs?k=' + playerKey + '&to=' + playerKey + '&data=',
      null,
    );
  }
}
