import {Component, OnDestroy, OnInit} from '@angular/core';
import {ApiService} from './service/api-service/api.service';
import {Subscription} from 'rxjs';
import {environment} from '../environments/environment';
import {GameObject} from './model/GameObject';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
}
