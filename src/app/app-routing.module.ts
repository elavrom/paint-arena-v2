import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GameComponent} from './core/game/game.component';
import {InitComponent} from './core/init/init.component';

const routes: Routes = [

  { path: '',
    redirectTo: '/init',
    pathMatch: 'full'
  },
  {
    path: 'init',
    component: InitComponent
  },
  {
    path: 'game',
    component: GameComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
