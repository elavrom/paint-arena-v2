import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {Player} from '../../model/Player';
import {ActivatedRoute} from '@angular/router';
import {environment} from '../../../environments/environment';
import {Subscription} from 'rxjs';
import {ApiService} from '../../service/api-service/api.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit, OnDestroy {
  public delta: number;
  public ping: number;


  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
  ) {
  }

  public canvas: HTMLCanvasElement;
  public canvasSize = 600;
  public numberOfCellsPerRow = 20;
  public cellSize = this.canvasSize / this.numberOfCellsPerRow;
  public gameMatrix;
  public times = [];
  public fps;

  public endAt;
  public timer;

  public subscriptions: Subscription[] = [];

  public romain = new Player('Romain', '#00FFFF', environment.romainKey);
  public seb = new Player('Seb', '#FF6625', environment.sebKey);

  public currentPlayer: Player;
  public otherPlayer: Player;

  public gameEnded = false;

  public static getCanvas(): HTMLCanvasElement {
    return <HTMLCanvasElement>document.getElementById('myCanvas');
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    this.movePlayer(event);
  }

  ngOnInit() {
    // @ts-ignore
    window.requestAnimationFrame =
      // @ts-ignore
      window.requestAnimationFrame || window.mozRequestAnimationFrame ||
      // @ts-ignore
      window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

    this.getKeysAndSetPlayers();
    this.subscriptions.push(this.apiService.clearPlayerQueue(this.currentPlayer.key).subscribe());
    this.getDeltaAndPingLoop();
    this.clockLoop();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => {
      if (sub) {
        sub.unsubscribe();
      }
    });
  }

  public getKeysAndSetPlayers() {
    return this.route.params.subscribe(params => {
      if (params['chosenKey'] === environment.romainKey.toString()) {
        this.currentPlayer = this.romain;
        this.otherPlayer = this.seb;
      } else if (params['chosenKey'] === environment.sebKey.toString()) {
        this.currentPlayer = this.seb;
        this.otherPlayer = this.romain;
      }
      this.endAt = params['endAt'];
      this.launchGame();
    });
  }

  private launchGame() {
    this.canvas = GameComponent.getCanvas();
    this.canvas.width = this.canvasSize;
    this.canvas.height = this.canvasSize;
    this.drawGrid();
    this.gameMatrix = this.initGameMatrix();
    this.initPlayerPositions();
    this.animationFrame();
  }

  public drawGrid() {
    const ctx = this.canvas.getContext('2d');
    const dx = this.cellSize;
    const dy = this.cellSize;

    let x = 0;
    let y = 0;
    const w = this.canvas.width;
    const h = this.canvas.height;

    let xy = 10;

    ctx.lineWidth = 1;
    ctx.strokeStyle = '#000000';

    while (y < h - this.cellSize) {
      y = y + dy;
      ctx.moveTo(x, y);
      ctx.lineTo(w, y);
      ctx.stroke();

      xy += 10;
    }

    y = 0;
    xy = 10;
    while (x < w - this.cellSize) {
      x = x + dx;
      ctx.moveTo(x, y);
      ctx.lineTo(x, h);
      ctx.stroke();

      xy += 10;
    }
  }

  /**
   * Let fill the cell only with its index in the matrix and the color we want
   * @param x x index
   * @param y y index
   * @param color color to fill the cell with
   */
  public fillCell(x, y, color) {
    const ctx = this.canvas.getContext('2d');
    const minIndex = 0;
    const maxIndex = this.numberOfCellsPerRow - 1;

    let rectY: number;
    let rectX: number;
    let rectWidth = this.cellSize - 3;
    let rectHeight = this.cellSize - 3;

    if (x !== minIndex && x !== maxIndex) {
      rectWidth -= 1;
    }

    if (x === minIndex) {
      rectX = 1;
    } else {
      rectX = x * this.cellSize + 2;
    }

    if (y !== minIndex && y !== maxIndex) {
      rectHeight -= 1;
    }

    if (y === minIndex) {
      rectY = 1;
    } else {
      rectY = y * this.cellSize + 2;
    }

    ctx.fillStyle = color;
    ctx.fillRect(rectX, rectY, rectWidth, rectHeight);

  }

  private getDeltaAndPing() {
    const ping = this.apiService.pingServer().subscribe(res => {
      const t3 = Date.now();
      this.delta = ((res['t1'] - res['t0']) + (res['t2'] - t3)) / 2;
      this.ping = (t3 - res['t0'] - (res['t2'] - res['t1']));
    }, error => {
    });
  }

  private getDeltaAndPingLoop() {
    setInterval(() => {
      this.getDeltaAndPing();
    }, 500);
  }

  // under 50 ms crash browser
  private clockLoop() {
    setInterval(() => {
      this.subscriptions.push(this.apiService.loadPlayerQueueInfo(this.currentPlayer.key).subscribe(data => {
        if (data['msg_qnum'] > 0) {
          this.subscriptions.push(this.apiService.getPlayerFirstQueueMessage(this.currentPlayer.key).subscribe(msg => {
            const pos = JSON.parse(msg['data']);
            this.gameMatrix[pos.oldX][pos.oldY] = this.otherPlayer.cellColor;
            this.otherPlayer.posX = pos.x;
            this.otherPlayer.posY = pos.y;
            this.gameMatrix[pos.x][pos.y] = this.otherPlayer.color;
          }));
        }
      }));
      this.fixColorMissingFrame();
    }, 50);
  }

  /**
   * Creates a matrix filled with zeroes, size depending on the number of cells wanted.
   */
  public initGameMatrix() {
    return Array.apply(
      null,
      new Array(this.numberOfCellsPerRow)).map(() =>
      Array.apply(null, new Array(this.numberOfCellsPerRow)).map(() => 0)
    );
  }

  public initPlayerPositions() {
    this.romain.posX = 0;
    this.romain.posY = 0;
    this.seb.posX = this.numberOfCellsPerRow - 1;
    this.seb.posY = this.numberOfCellsPerRow - 1;
    this.gameMatrix[0][0] = this.romain.color;
    this.gameMatrix[this.numberOfCellsPerRow - 1][this.numberOfCellsPerRow - 1] = this.seb.color;
  }

  public fillGrid() {
    for (let _x = 0; _x < this.gameMatrix.length; _x++) {
      for (let _y = 0; _y < this.gameMatrix[_x].length; _y++) {
        if (this.gameMatrix[_x][_y] !== 0) {
          this.fillCell(_x, _y, this.gameMatrix[_x][_y]);
        }
      }
    }
  }

  /**
   * This function is called at each frame (60-75/s)
   */
  public animationFrame() {
    if ((this.endAt - Date.now()) / 1000 > 0) {
      this.fixColorMissingFrame();
      this.timer = (this.endAt - Date.now() + this.delta) / 1000;
      this.calculateFPS();
      this.fillGrid();
      requestAnimationFrame(() => this.animationFrame());
    } else {
      this.timer = 0;
      this.calculateScore();
    }
  }

  public sendMyPosition(oldX, oldY, newX, newY) {
    this.subscriptions.push(this.apiService.pushPosition(
      this.otherPlayer.key, JSON.stringify({oldX: oldX, oldY: oldY, x: newX, y: newY})
    ).subscribe());
  }

  public calculateFPS() {
    const now = performance.now();
    while (this.times.length > 0 && this.times[0] <= now - 1000) {
      this.times.shift();
    }
    this.times.push(now);
    this.fps = this.times.length;
  }

  public movePlayer(event) {
    if (!this.gameEnded) {
      if (event.key === 'z') {
        if (this.currentPlayer.posY > 0 &&
          (this.currentPlayer.posY - 1 !== this.otherPlayer.posY
            || this.currentPlayer.posX !== this.otherPlayer.posX)) {
          const oldX = this.currentPlayer.posX;
          const oldY = this.currentPlayer.posY;
          this.gameMatrix[this.currentPlayer.posX][this.currentPlayer.posY] = this.currentPlayer.cellColor;
          this.gameMatrix[this.currentPlayer.posX][this.currentPlayer.posY - 1] = this.currentPlayer.color;
          this.currentPlayer.posY -= 1;
          this.sendMyPosition(oldX, oldY, this.currentPlayer.posX, this.currentPlayer.posY);
        }
      }

      if (event.key === 's') {
        if (this.currentPlayer.posY < this.numberOfCellsPerRow - 1 &&
          (this.currentPlayer.posY + 1 !== this.otherPlayer.posY
            || this.currentPlayer.posX !== this.otherPlayer.posX)) {
          const oldX = this.currentPlayer.posX;
          const oldY = this.currentPlayer.posY;
          this.gameMatrix[this.currentPlayer.posX][this.currentPlayer.posY] = this.currentPlayer.cellColor;
          this.gameMatrix[this.currentPlayer.posX][this.currentPlayer.posY + 1] = this.currentPlayer.color;
          this.currentPlayer.posY += 1;
          this.sendMyPosition(oldX, oldY, this.currentPlayer.posX, this.currentPlayer.posY);
        }
      }

      if (event.key === 'q') {
        if (this.currentPlayer.posX > 0 &&
          (this.currentPlayer.posX - 1 !== this.otherPlayer.posX
            || this.currentPlayer.posY !== this.otherPlayer.posY)) {
          const oldX = this.currentPlayer.posX;
          const oldY = this.currentPlayer.posY;
          this.gameMatrix[this.currentPlayer.posX][this.currentPlayer.posY] = this.currentPlayer.cellColor;
          this.gameMatrix[this.currentPlayer.posX - 1][this.currentPlayer.posY] = this.currentPlayer.color;
          this.currentPlayer.posX -= 1;
          this.sendMyPosition(oldX, oldY, this.currentPlayer.posX, this.currentPlayer.posY);
        }
      }

      if (event.key === 'd') {
        if (this.currentPlayer.posX < this.numberOfCellsPerRow - 1 &&
          (this.currentPlayer.posX + 1 !== this.otherPlayer.posX
            || this.currentPlayer.posY !== this.otherPlayer.posY)) {
          const oldX = this.currentPlayer.posX;
          const oldY = this.currentPlayer.posY;
          this.gameMatrix[this.currentPlayer.posX][this.currentPlayer.posY] = this.currentPlayer.cellColor;
          this.gameMatrix[this.currentPlayer.posX + 1][this.currentPlayer.posY] = this.currentPlayer.color;
          this.currentPlayer.posX += 1;
          this.sendMyPosition(oldX, oldY, this.currentPlayer.posX, this.currentPlayer.posY);
        }
      }
    }
  }

  public calculateScore() {
    this.gameEnded = true;
    this.subscriptions.push(this.apiService.loadPlayerQueueInfo(this.currentPlayer.key).subscribe(data => {
      if (data['msg_qnum'] > 0) {
        for (let i = 0; i < data['msg_qnum'].length; i++) {
          this.subscriptions.push(this.apiService.getPlayerFirstQueueMessage(this.currentPlayer.key).subscribe(msg => {
            const pos = JSON.parse(msg['data']);
            this.gameMatrix[pos.oldX][pos.oldY] = this.otherPlayer.cellColor;
            this.otherPlayer.posX = pos.x;
            this.otherPlayer.posY = pos.y;
            this.gameMatrix[pos.x][pos.y] = this.otherPlayer.color;
          }));
        }
        this.fixColorMissingFrame();
        this.gameMatrix.forEach(column => {
          column.forEach(value => {
            if (value === this.romain.cellColor || value === this.romain.color) {
              this.romain.score++;
            } else if (value === this.seb.cellColor || value === this.seb.color) {
              this.seb.score++;
            }
          });
        });
      }
    }));
  }

  public fixColorMissingFrame() {
    this.gameMatrix.forEach((column, x) => {
      column.forEach((value, y) => {
        if (value === this.otherPlayer.color && x !== this.otherPlayer.posX && y !== this.otherPlayer.posY) {
          this.gameMatrix[x][y] = this.otherPlayer.cellColor;
        }
      });
    });
  }
}


