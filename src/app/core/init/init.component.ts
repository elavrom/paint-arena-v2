import {Component, OnDestroy, OnInit} from '@angular/core';
import {GameObject} from '../../model/GameObject';
import {Subscription} from 'rxjs';
import {environment} from '../../../environments/environment';
import {ApiService} from '../../service/api-service/api.service';
import {isNullOrUndefined} from 'util';
import {Router} from '@angular/router';
import {Player} from '../../model/Player';

@Component({
  selector: 'app-init',
  templateUrl: './init.component.html',
  styleUrls: ['./init.component.scss']
})
export class InitComponent implements OnInit, OnDestroy {

  title = 'PaintArenaV2';


  public game: GameObject;
  public chosenKey: number;
  public opponentKey: number;
  public romainKey: number;
  public sebKey: number;
  public delta: number;
  public ping: number;
  public timer: number;
  public subscriptions: Subscription[];

  ngOnInit(): void {
    this.romainKey = environment.romainKey;
    this.sebKey = environment.sebKey;
    this.deltaPingLoop();
    this.clockLoop();
  }

  constructor(
    private apiService: ApiService,
    private router: Router,
  ) {
    this.subscriptions = [];
    this.game = GameObject.default();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => {
      if (sub) {
        sub.unsubscribe();
      }
    });
  }

  // Distinct loop as we need delta and ping updated more often
  // to limit the time shift between players
  private deltaPingLoop() {
    setInterval(() => {
      this.getDeltaAndPing();
    }, 1500);
  }

  // under 500 ms fuck up everything
  private clockLoop() {
    setInterval(() => {
      this.tryLoadGame();
    }, 500);
  }


  private getDeltaAndPing() {
    const ping = this.apiService.pingServer().subscribe(res => {
      const t3 = Date.now();
      this.delta = ((res['t1'] - res['t0']) + (res['t2'] - t3)) / 2;
      this.ping = (t3 - res['t0'] - (res['t2'] - res['t1']));
    }, error => {
    });
  }

  private tryLoadGame() {
    if (isNullOrUndefined(this.chosenKey)) {
      this.subscriptions.push(this.apiService.loadPlayerQueueInfo(this.romainKey).subscribe(data => {
        if (data['msg_qnum'] > 0) {
          this.opponentKey = this.sebKey;
          this.game.sebReady = true;
          this.subscriptions.push(this.apiService.clearPlayerQueue(this.romainKey).subscribe());
        } else {
          this.subscriptions.push(this.apiService.loadPlayerQueueInfo(this.sebKey).subscribe(data2 => {
            if (data2['msg_qnum'] > 0) {
              this.opponentKey = this.romainKey;
              this.game.romainReady = true;
              this.subscriptions.push(this.apiService.clearPlayerQueue(this.sebKey).subscribe());
            }
          }));
        }
      }));
    } else {
      this.subscriptions.push(this.apiService.loadPlayerQueueInfo(this.chosenKey).subscribe(data => {
        if (data['msg_qnum'] > 0) {
          this.subscriptions.push(this.apiService.getPlayerFirstQueueMessage(this.chosenKey).subscribe(data2 => {
            const msg = JSON.parse(data2['data']);
            if (msg.start === true) {
              if (this.game.romainReady) {
                this.game.sebReady = true;
              } else if (this.game.sebReady) {
                this.game.romainReady = true;
              }
              this.opponentKey = this.chosenKey === this.sebKey ? this.romainKey : this.sebKey;
              // permet de démarrer en meme temps
              setInterval(() => {
                this.timer = (msg.startAt - Date.now() + this.delta) / 1000;
              }, 10);

              setTimeout(() => this.goToGameState(), msg.startAt - Date.now() + this.delta);
            }
          }));

        }
      }));
    }
  }

  private goToGameState() {
    this.router.navigate(['game', {chosenKey: this.chosenKey, opponentKey: this.opponentKey, endAt: Date.now() + 60000 + this.delta}]);
  }

  public choosePlayer(playerKey) {
    // Il faut ne pas avoir choisi de clé et que l'autre soit dispo
    if (isNullOrUndefined(this.chosenKey)) {
      if (playerKey === environment.sebKey && !this.game.sebReady) {
        this.game.sebReady = true;
        this.chosenKey = this.sebKey;
        if (isNullOrUndefined(this.opponentKey)) {
          this.subscriptions.push(this.apiService.sendEmptyMessage(this.romainKey).subscribe());
        } else {
          this.sendStartMessage();
        }
      } else if (playerKey === environment.romainKey && !this.game.romainReady) {
        this.game.romainReady = true;
        this.chosenKey = this.romainKey;
        if (isNullOrUndefined(this.opponentKey)) {
          this.subscriptions.push(this.apiService.sendEmptyMessage(this.sebKey).subscribe());
        } else {
          this.sendStartMessage();
        }
      }
    } else {
      this.sendStartMessage();
    }
  }

  private sendStartMessage() {
    const msg = JSON.stringify({start: true, startAt: Date.now() + 5000 + this.delta});
    this.subscriptions.push(this.apiService.sendMessage(this.chosenKey, msg).subscribe());
    this.subscriptions.push(this.apiService.sendMessage(this.opponentKey, msg).subscribe());
  }

}
