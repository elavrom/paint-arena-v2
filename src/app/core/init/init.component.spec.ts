import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitComponent } from './init.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('InitComponent', () => {
  let component: InitComponent;
  let fixture: ComponentFixture<InitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      declarations: [ InitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have as title 'PaintArenaV2'`, () => {
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('PaintArenaV2');
  });

  it('should render title in a h1 tag', () => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to PaintArenaV2!');
  });
});
