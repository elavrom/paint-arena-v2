import {Tools} from '../tools/tools';

export class Player {

  private _name: string;
  private _color: string;
  private _cellColor: string;
  private _posX: number;
  private _posY: number;
  private _key: number;
  private _score = 0;


  constructor(
    name: string,
    color: string,
    key: number,
  ) {
    this._name = name;
    this._color = color;
    this._cellColor = Tools.ColorLuminance(this.color, -0.5);
    this._key = key;
  }

  get key(): number {
    return this._key;
  }

  set key(value: number) {
    this._key = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get color(): string {
    return this._color;
  }

  set color(value: string) {
    this._color = value;
  }

  get cellColor(): string {
    return this._cellColor;
  }

  set cellColor(value: string) {
    this._cellColor = value;
  }

  get posX(): number {
    return this._posX;
  }

  set posX(value: number) {
    this._posX = value;
  }

  get posY(): number {
    return this._posY;
  }

  set posY(value: number) {
    this._posY = value;
  }

  get score(): number {
    return this._score;
  }

  set score(value: number) {
    this._score = value;
  }
}
