export class GameObject {
  id: number;
  romainReady: boolean;
  sebReady: boolean;

  constructor(
    id: number,
    romainReady: boolean,
    sebReady: boolean,
  ) {
    this.id = id;
    this.romainReady = romainReady;
    this.sebReady = sebReady;
  }

  static default() {
    return new GameObject(0, false, false);
  }
}
