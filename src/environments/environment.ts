// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// export const environment = {
//   production: false,
//   initKey: 255060,
//   romainKey: 255051,
//   sebKey: 255060,
//   baseApi: 'http://syllab.com/PTRE839/',
// };

export const environment = {
  production: false,
  initKey: 123,
  romainKey: 456,
  sebKey: 123,
  gameObjKey: 123, // Contains Game Data
  gameIdKey: 456, // Contains Game ID (latest update)
  baseApi: 'http://rtplay.local/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
