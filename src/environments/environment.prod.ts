export const environment = {
  production: true,
  initKey: 123,
  romainKey: 456,
  sebKey: 123,
  gameObjKey: 123, // Contains Game Data
  gameIdKey: 456, // Contains Game ID (latest update)
  baseApi: 'http://rtplay.romainvaleye.ovh/',
};
